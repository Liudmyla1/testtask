import java.util.Random;

public class Square extends Figure {
	Square() {
		type = "square";
		Random rand = new Random();
		a = rand.nextInt(1000)+1;
	}
	int a;
	
	int getSide() {
		return a;
	}
	
	int getArea() {
		return a*a;
	}
	
	void draw() {
		
	}
}
