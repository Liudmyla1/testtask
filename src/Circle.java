import java.util.Random;

public class Circle extends Figure{
	Circle() {
		type = "circle";
		Random rand = new Random();
		radius = rand.nextInt(1000)+1;
	}
	int radius;
	
	int getRadius() {
		return radius;
	}
	
	int getArea() {
		return (int)(radius*radius*Math.PI);
	}
	
	void draw() {
		
	}
}
