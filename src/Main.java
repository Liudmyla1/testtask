
import java.util.Random;
import java.util.Scanner;

public class Main {
	
	private static Scanner scanner;

	public static void main(String[] args) {
		System.out.println("Enter the number of figures: ");
		scanner = new Scanner(System.in);
		int k = scanner.nextInt();
		Figure[] mass = new Figure[k];
		for (int i = 0; i<k; i++) {
			Random rand = new Random();
			int randFigure = rand.nextInt(4) + 1;
			switch(randFigure) {
			case 1:
				mass[i] = new Square();
				break;
			case 2:
				mass[i] = new Triangle();
				break;
			case 3:
				mass[i] = new Circle();
				break;
			case 4:
				mass[i] = new Trapezium();
				break;
			}
			mass[i].setColor();
		}
		
		for (int i = 0; i<k; i++) {
			switch(mass[i].getType()) {
			case "square":
				System.out.println("Figure: " + mass[i].getType() + ", area: " +	mass[i].getArea() 
				+ " sq. units, side length: " + ((Square) mass[i]).getSide() + " units, color: " + mass[i].getColor());
				break;
			case "triangle":
				System.out.println("Figure: " + mass[i].getType() + ", area: " +	mass[i].getArea() 
				+ " sq. units, hypotenuse: " + ((Triangle) mass[i]).getHypotenuse() + " units, color: " + mass[i].getColor());
				break;
			case "circle":
				System.out.println("Figure: " + mass[i].getType() + ", area: " +	mass[i].getArea() 
				+ " sq. units, radius: " + ((Circle) mass[i]).getRadius() + " units, color: " + mass[i].getColor());
				break;
			case "trapezium":
				System.out.println("Figure: " + mass[i].getType() + ", area: " +	mass[i].getArea() 
				+ " sq. units, height: " + ((Trapezium) mass[i]).getHeight() + " units, color: " + mass[i].getColor());
				break;
			}
		}
	}
}
