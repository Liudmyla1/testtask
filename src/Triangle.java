import java.text.DecimalFormat;
import java.util.Random;

public class Triangle extends Figure{
	Triangle(){
		type = "triangle";
		Random rand = new Random();
		a = rand.nextInt(1000)+1;
		b = rand.nextInt(1000)+1;
	}
	int a, b;
	
	String getHypotenuse() {
		DecimalFormat df = new DecimalFormat("#.###");
		return df.format(Math.sqrt(a*a+b*b));
	}
	
	int getArea() {	
		return a*b/2;
	}

	void draw() {
		
	}
}
