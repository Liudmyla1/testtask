import java.util.Random;
import java.awt.Color;

public class Figure {
	Figure(){
		
	}
	String type;
	Color color;
	String colorName;
	
	void setColor() {
		Random rand = new Random();
		int randC = rand.nextInt(5)+1;
		int R=0;
		int G=0;
		int B=0;
		switch(randC) {
		case 1:
			R = 255;
			G = 0;
			B = 0;
			colorName = "red";
			break;
		case 2:
			R = 0;
			G = 255;
			B = 0;
			colorName = "green";
			break;
		case 3:
			R = 0;
			G = 0;
			B = 255;
			colorName = "blue";
			break;
		case 4:
			R = 255;
			G = 255;
			B = 0;
			colorName = "yellow";
			break;
		case 5:
			R = 255;
			G = 165;
			B = 0;
			colorName = "orange";
			break;
		}
		color = new Color(R, G, B);
	}
	
	void draw(){
		
	}
	int getArea(){
		return 0;
	}
	String getColor(){
		return colorName;
	}
	String getType(){
		return type;
	}
}
