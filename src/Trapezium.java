import java.util.Random;

public class Trapezium extends Figure{
	Trapezium(){
		type = "trapezium";
		Random rand = new Random();
		a = rand.nextInt(1000)+1;
		b = rand.nextInt(1000)+1;
		height = rand.nextInt(1000)+1;
	}
	int a, b, height;
	
	int getHeight() {
		return height;
	}
	
	int getArea() {
		return (a+b)*height/2;
	}
	
	void draw() {
		
	}
}
